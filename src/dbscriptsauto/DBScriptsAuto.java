package dbscriptsauto;

import java.io.File;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author critis1
 */
public class DBScriptsAuto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\critis1\\Selenium\\chromedriver_win32\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();  
        options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors");  
        WebDriver driver = new ChromeDriver(options);
        driver.navigate().to("http://confluence.critis.gr:8191/login.action?os_destination=%2Findex.action&permissionViolation=true");

        // Login
        WebElement username = driver.findElement(By.id("os_username"));
        WebElement password = driver.findElement(By.id("os_password"));
        WebElement loginButton = driver.findElement(By.id("loginButton"));
        username.sendKeys("stefania");
        password.sendKeys("st3f1@critis");
        loginButton.click();

        openFile(driver);
        emptyTxtFile();
        driver.quit();
    }

    public static void emptyTxtFile() throws IOException {
        FileChannel.open(Paths.get("C:\\Users\\critis1\\CRITIS_DB_SCRIPTS\\New_Scripts.txt"), StandardOpenOption.WRITE).truncate(0).close();
    }

    public static void copyDataPagni(List<String> list, WebDriver driver, File file) throws IOException {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(DBScriptsAuto.class.getName()).log(Level.SEVERE, null, ex);
        }

        driver.navigate().to("http://confluence.critis.gr:8191/display/CS/PAGNI+ICU");
        WebElement editBtn2 = driver.findElement(By.id("editPageLink"));
        editBtn2.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(DBScriptsAuto.class.getName()).log(Level.SEVERE, null, ex);
        }

        new WebDriverWait(driver, 20).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[contains(@id,'wysiwygTextarea_ifr')]")));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        for (String str : list) {
            String code
                    = "body = document.querySelector('body');"
                    + "element = document.createElement('p');"
                    + "text = document.createTextNode('" + str + "');"
                    + "element.appendChild(text);"
                    + "body.append(element);";
            js.executeScript(code);
        }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            Logger.getLogger(DBScriptsAuto.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.switchTo().defaultContent();
        WebElement updateBtn2 = driver.findElement(By.id("rte-button-publish"));
        updateBtn2.click();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            Logger.getLogger(DBScriptsAuto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void copyDataLarisa(List<String> list, WebDriver driver, File file) throws IOException {
        driver.navigate().to("http://confluence.critis.gr:8191/display/CS/GN+LARISA+ICU");
        WebElement editBtn = driver.findElement(By.id("editPageLink"));
        editBtn.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(DBScriptsAuto.class.getName()).log(Level.SEVERE, null, ex);
        }
        new WebDriverWait(driver, 20).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[contains(@id,'wysiwygTextarea_ifr')]")));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try {
            Thread.sleep(8000);
        } catch (InterruptedException ex) {
            Logger.getLogger(DBScriptsAuto.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (String str : list) {
            String code
                    = "body = document.querySelector('body');"
                    + "element = document.createElement('p');"
                    + "text = document.createTextNode('" + str + "');"
                    + "element.appendChild(text);"
                    + "body.append(element);";
            js.executeScript(code);
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            Logger.getLogger(DBScriptsAuto.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.switchTo().defaultContent();
        WebElement updateBtn = driver.findElement(By.id("rte-button-publish"));
        updateBtn.click();

        WebElement editBtn2;
        WebDriverWait wait = new WebDriverWait(driver, 50);
        editBtn2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("editPageLink")));

    }

    public static void openFile(WebDriver driver) throws IOException {

        File file = new File("C:\\Users\\critis1\\CRITIS_DB_SCRIPTS\\New_Scripts.txt");
        if (file.exists() && !file.isDirectory()) {
            if (file.length() > 0) {
                Scanner fileInput = new Scanner(file);
                List<String> list = new ArrayList();
                while (fileInput.hasNext()) {
                    String nextLine = fileInput.nextLine();
                    String replacedQuotes = nextLine.replaceAll("'", "\"");
                    StringBuilder builder = new StringBuilder();
                    for (char c : replacedQuotes.toCharArray()) {
                        switch (c) {
                            case '"':
                                builder.append('\\');
                            default:
                                builder.append(c);
                        }
                    }
                    String converted = builder.toString();
                    list.add(converted);
                }
                copyDataLarisa(list, driver, file);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(DBScriptsAuto.class.getName()).log(Level.SEVERE, null, ex);
                }
                copyDataPagni(list, driver, file);
            } else {
                System.out.println("File is empty.");
            }
        }

    }
}
